import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/about',
            name: 'about',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
        },
        {
            path: '/pl-table',
            name: 'pl-table',
            component: () => import('./views/PLTable.vue')
        },
        {
            path: '/players',
            name: 'players',
            component: () => import('./views/Players.vue')
        },
        {
            path: '/predictions',
            name: 'predictions',
            component: () => import('./views/Predictions.vue')
        },
        {
            path: '/scoring',
            name: 'scoring',
            component: () => import('./views/Scoring.vue')
        }
    ]
})
